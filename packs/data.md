# Data Packs

----

## Compilations

<a href="https://vanillatweaks.net/picker/crafting-tweaks/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/data/vanilla-tweaks.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Vanilla Tweaks (Crafting)</a>
These only change crafting recipes.

<a href="https://vanillatweaks.net/picker/datapacks/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/data/vanilla-tweaks.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Vanilla Tweaks (Functionality)</a>
These add new features, very easy to select which ones you want.

<a href="http://mc.voodoobeard.com/#datapacks"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/data/voodoobeard.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Voodoo Packs</a>
Adds some more features which are kinda neat.

----

## World Generation

<a href="https://www.planetminecraft.com/data-pack/neo-beta-datapack-beta-1-7-3-esque-terrain-generation-for-your-1-16-2-worlds/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/data/neobeta.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Neo Beta Pack</a>
Makes terrain generation very close to beta1.7.3

<a href="https://www.planetminecraft.com/data-pack/skylands-reborn/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/data/skylands.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Skylands Reborn</a>
Aether-like dimension, completely Vanilla.

### Structures

<a href="https://www.planetminecraft.com/data-pack/brick-pyramids/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/data/brick-pyramid.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Brick Pyramids</a>
Infdev structures from very old times.

<a href="https://www.curseforge.com/minecraft/customization/fancier-mansions"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/data/fancier-mansions.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Fancier Mansions</a>
Makes Mansions more interesting and beautiful.

----

## Other

<a href="https://www.planetminecraft.com/data-pack/found-items-useful-wandering-traders/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/data/found-items.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Wandering Trader sells Found Items</a>
They sell you back items which you have lost due to dying.

<a href="https://www.curseforge.com/minecraft/customization/merge-xp-orbs"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/data/merge-xp-orbs.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Merge XP orbs</a>
Reduces lag and makes picking orbs way faster.
