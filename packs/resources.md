
# Resource Packs

----

## Compilations

<a href="https://vanillatweaks.net/picker/resource-packs/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/vanilla-tweaks.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Vanilla Tweaks</a>
They just have many features which can be selected in the browser before download into one zip archive.

<a href="https://compliancepack.net/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/compliance.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Compliance</a>
Spiritual sucessor to Faithful. <a href="https://www.youtube.com/watch?v=kgZYBTlwaVs">Why?</a>

----

## Visuals

<a href="https://www.curseforge.com/minecraft/texture-packs/flower-pots"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/flowerpots.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Flower Pots +</a>
Changes the soil and light of potted plants.

<a href="https://www.curseforge.com/minecraft/texture-packs/animated-furnaces"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/animated-furnaces.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Animated Furnaces</a>
Better shows if they are working, and fixes some visual stuff.

<a href="https://www.curseforge.com/minecraft/texture-packs/improved-bottles-o-enchanting-texture-splash-form"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/improved-enchanting-bottles.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Improved Enchanting Bottles</a>
Changes it to splash potion form, and animates it with EXP orbs flowing around.

<a href="https://www.planetminecraft.com/texture-pack/colored-subtitles"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/colored-subtitles.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Colored Subtitles</a>
In order to better get a sense of the situation.

<a href="https://www.curseforge.com/minecraft/texture-packs/gui-2d"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/2d-blocks-inv.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> 2D Block for Inventory</a>
Makes the inventory look like a wall.

<a href="https://www.curseforge.com/minecraft/texture-packs/update-edition"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/update-edition.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Update Edition</a>
Changes "Java Edition" to "Caves & Cliffs" for example.

----

## 3D

<a href="https://www.curseforge.com/minecraft/texture-packs/spawn-egg-3d"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/spawn-egg-3d.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Spawn Egg 3D</a>
Changes Spawn Eggs to be a 3D model of the mob, easy to identify at first glance.

<a href="https://www.planetminecraft.com/texture-pack/lilypad-5349735/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/lilypad.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Lilypad+</a>
Better and varied Lilypads.

----

## CIT

<i>These either require MCPatcher, Optifine, CIT Resewn or yet another mod for Custom Items</i>

<a href="https://www.curseforge.com/minecraft/texture-packs/different-textures-for-enchanted-books"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/diff-enchanted-books.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Different Enchanted Books</a>
Gives each book a unique appearance with some style rules. Really useful for chests filled with them.

<a href="https://www.curseforge.com/minecraft/texture-packs/color-corkination"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/color-corkination.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Color Corkination</a>
Gives potion's bottle corks different colors for if they are extended or powered. Also un-sus your flower stews.

----

## Extra Sensorial Perception

<a href="https://www.curseforge.com/minecraft/texture-packs/xray-ultimate-1-11-compatible"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/res/xray-ultimate.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> X-Ray Ultimate</a>
It's just really useful sometimes, besides if the server is good, you can't really abuse it.
