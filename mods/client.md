# Client mods

<a href="https://fabricmc.net/use/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/fabric.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Fabric</a>
The mod loader, you need this.

----

## API, Configs, Deps, etc.

<a href="https://modrinth.com/mod/fabric-api"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/fabric.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Fabric API</a>
A lot of mods use it, so everyone just assumes you have it.<br>

<a href="https://modrinth.com/mod/modmenu"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/modmenu.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Mod Menu</a>
To configure everything in-game.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/cloth-config"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/clothconfig.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Cloth Config</a>
Apparently also used by many mods.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/cloth-api"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/clothconfig.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Cloth API</a>
Complements Cloth-Config.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/kotlin.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Kotlin</a>
Allows the usage of the Kotlin language for some mods.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/architectury-fabric"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/architectury.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Architectury</a>
Required by some GUI mods.<br>

<a href="https://modrinth.com/mod/indium"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/indium.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Indium</a>
Required if you have Sodium and a mod needs the Fabric Rendering API.<br>

----

## Visual Performance

<a href="https://modrinth.com/mod/sodium"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/sodium.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Sodium</a>
Massive rendering performance overhaul.<br>

<a href="https://modrinth.com/mod/sodium-extra"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/sodiumextra.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Sodium Extra</a>
Some useful options for Sodium, like no-fog or not rendering moving pistons.<br>

<a href="https://modrinth.com/mod/cull-leaves"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/cullleaves.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Cull Leaves</a>
Makes leaves don't lag if you can't see them.<br>

<a href="https://modrinth.com/mod/ebe"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/ebe.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Enhanced Block Entities</a>
Reduces lag caused by block entities.<br>

<a href="https://modrinth.com/mod/entity-view-distance"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/evd.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Entity View Distance</a>
Changes individual entities rendering distances.<br>

----

## Optifine Replacement

<a href="https://modrinth.com/mod/ok-zoomer"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/okzoomer.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> OK Zoomer</a>
Adds zooming functionality without the Spy-Glass.<br>

<a href="https://modrinth.com/mod/fabrishot"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/fabrishot.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> FabriShot</a>
Allows to take really large screenshots<br>

<a href="https://modrinth.com/mod/cit-resewn"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/cit.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> CIT Resewn</a>
Support for Custom Items.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/custom-entity-models-cem"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/cem.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Custom Entity Models (CEM)</a>
Support for Custom mobs, proyectiles, and more.<br>

<a href="https://modrinth.com/mod/animatica"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/animatica.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Animatica</a>
Allows for more textures to be animated.<br>

<a href="https://modrinth.com/mod/capes"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/capes.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Capes</a>
Shows players' OptiFine, LabyMod and MinecraftCapes mod cape.<br>

----

## GUI

<a href="https://www.curseforge.com/minecraft/mc-mods/slight-gui-modifications"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/sgm.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> 'Slight' GUI Modifications</a>
Changes vastly some GUI parts, very configurable.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/smooth-scrolling-everywhere-fabric"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/sse.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Smooth Scrolling Everywhere</a>
Makes scrolling less painful to the eyes.<br>

<a href="https://modrinth.com/mod/brb"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/brb.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Better Recipe Book</a>
Enhances the Recipe book, also adding Brewing recipes.<br>

<a href="https://modrinth.com/mod/inspecio"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/inspecio.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Inspecio</a>
Makes tooltips way more verbose and informative.<br>

----

## HUD

<a href="https://modrinth.com/mod/enhanced-attack-indicator"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/eai.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Enhanced Attack Indicator</a>
Allows to use the Attack Indicator for block breaking progress, bow pulling and more.<br>

<a href="https://modrinth.com/mod/betterf3"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/betterf3.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> BetterF3</a>
Colours the Debug HUD and you can also customize it, so you can see exactly what you want.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/appleskin"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/appleskin.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> AppleSkin</a>
More informative Hunger overlay.<br>

<a href="https://modrinth.com/mod/pinglist"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/pinglist.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> PingList</a>
Shows the numeric ping, instead of bars.<br>

<a href="https://modrinth.com/mod/removehud"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/rhbnh.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Remove HUD but not Hand</a>
Increases inmersion, also allows to remove individually.<br>

<a href="https://modrinth.com/mod/smarthudreheated"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/smart-hud-reheated.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Smart HUD Reheated</a>
Shows clock, compass, arrows and armour in your "wristwatches".<br>


----

## Eye Candy

<a href="https://modrinth.com/mod/iris"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/iris.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Iris Shaders</a>
Sodium Addon that allows using Shaders.<br>

<a href="https://modrinth.com/mod/continuity"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/continuity.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Continuity</a>
Adds Connected Textures.<br>

<a href="https://modrinth.com/mod/ears"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/ears.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Ears</a>
Additional pixels on your body, doesn't require an external server.<br>

----

## Audio Enhancements

<a href="https://www.curseforge.com/minecraft/mc-mods/pling"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/pling.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Pling</a>
Audio cue that it has finished loading.<br>

<a href="https://modrinth.com/mod/chatsounds"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/chatsounds.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Chat Sounds</a>
Makes sounds when people chat.<br>

----

## Other

<a href="https://modrinth.com/mod/dynamic-fps"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/dfps.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Dynamic FPS</a>
Limits FPS when unfocused, so your PC doesn't struggle while switching.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/better-third-person"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/b3p.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Better Third Person</a>
Allows to rotate over the limits and move independant of the camera.<br>

<a href="https://modrinth.com/mod/here-be-no-dragons"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/hbnd.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Here be no Dragons!</a>
Disables the warning of custom maps.<br>

<a href="https://modrinth.com/mod/dont-drop-it"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/ddi.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Don't Drop it!</a>
Makes it harder to accidentaly drop your stuff.<br>

<a href="https://modrinth.com/mod/notenoughcrashes"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/nec.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Not Enough Crashes</a>
Helps if the game crashes, as it gives info and returns you to the main menu.<br>

<a href="https://modrinth.com/mod/fabrictailor"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/fabrictailor.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Fabric Tailor</a>
Allows in-game usage of any skin, useful for "not online" servers.<br>

<a href="https://modrinth.com/mod/multiconnect"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/multiconnect.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Multiconnect</a>
Allows you to login into older version servers, also mods your movement for pre 1.13 swiming.<br>


<a href="https://modrinth.com/mod/tiefix"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/tiefix.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> TieFix</a>
Fixes many bugs<br>

----

## Extra Sensorial Perception

<a href="https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/xaero.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Xaero's Minimap</a>
Minimap and Waypoints.<br>

<a href="https://modrinth.com/mod/nbttooltip"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/nbttooltip.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> NBT Tooltip</a>
Shows 'hidden' data used by items, can help with CIT development.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/autofish"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/autofish.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> AutoFish</a>
Automatically reels and recasts.<br>

<a href="https://github.com/19MisterX98/SeedcrackerX/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/seedcrackerx.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> SeedCrackerX</a>
Find the seed of the world.<br>

