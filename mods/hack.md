# Hack Utilities

<i>Disclaimer: Use these at your own risk, don't use them in non-anarchy servers. It's best if you try them in singleplayer, with a server of yours and only then join an anarchy server.</i>

----

## Open Source
|Name|Forge|Fabric|
--- | --- | ---
|<a href="https://inertiaclient.com/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/inertia.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Inertia</a>|1.12.2|1.14.4 1.15.2 1.16.5|
|<a href="https://aresclient.org/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/ares.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Ares</a>|1.12.2|1.16.5 1.17.1 1.18.1|
 | | 
|<a href="https://github.com/momentumdevelopment/cosmos"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/cosmos.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Cosmos</a>|1.12.2||
|<a href="https://github.com/WurstPlus/wurst-plus-three"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/wurstp3.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Wurst+3</a>|1.12.2||
|<a href="https://github.com/fr1kin/ForgeHax"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/forgehax.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> ForgeHax</a>|1.12.2 1.16.5||

----

## Closed Source
|Name|Versions|
| --- | --- |
|<a href="https://impactclient.net/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/impact.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Impact</a>|1.12.2 1.13.2 1.14.4 1.15.2 1.16.5|
