# Speedrunning mods

<i>This website has the links for 1.16.1, which is the prefered version to run</i>

<a href="https://fabricmc.net/use/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/fabric.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Fabric</a>
The mod loader, you need this.

----

## Performance

<a href="https://github.com/mrmangohands/sodium-fabric/releases"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/sodium.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Sodium</a>
Massive rendering performance overhaul<br>

<a href="https://github.com/mrmangohands/lithium-fabric/releases"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/lithium.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Lithium</a>
Improves TPS.<br>

<a href="https://modrinth.com/mod/starlight"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/starlight.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Starlight</a>
Changes the lighting engine to be faster.<br>

<a href="https://modrinth.com/mod/lazydfu"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/lazydfu.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> LazyDFU</a>
Makes "Data Fixer Upper" work only when neccesary.<br>

<a href="https://modrinth.com/mod/krypton"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/krypton.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Krypton</a>
Improves the Networking Stack.<br>


----

## Quality of Life

<a href="https://modrinth.com/mod/speedrunigt"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/krypton.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> SpeedRun IGT</a>
Shows an accurate timer.<br>

<a href="https://github.com/VoidXWalker/extra-options/releases"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/extra-options.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Extra Options</a>
Backports the Accesibility options.<br>

<a href="https://github.com/jan-leila/FastReset/releases/tag/1.16.1-1.4.0"> Fast Reset</a>
Can quit the world without saving (faster, time travel).<br>

<a href="https://github.com/DuncanRuns/AutoResetMod/releases"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/autoreset.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Auto Reset</a>
Creates a new world when quitting.<br>

<a href="https://github.com/VoidXWalker/NoPeaceful/releases"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/nopeaceful.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> No Peaceful</a>
Disables switching to peaceful mode.<br>


----

## Other

<a href="https://github.com/modmuss50/Voyager/releases/tag/1.0.0"> Voyager</a>
Fixes MC-149777 which started in 1.14 and was fixed for 1.17 .<br>


<a href="https://modrinth.com/mod/lotas"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/lotas.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> LoTAS</a>
The Tool for Tool Assisted Speedruns.<br>

