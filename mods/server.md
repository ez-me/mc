# Server and Singleplayer

<a href="https://fabricmc.net/use/"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/fabric.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Fabric</a>
The mod loader, you need this

----

## API, Dependencies, etc.

<a href="https://modrinth.com/mod/fabric-api"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/fabric.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Fabric API</a>
A lot of mods use it, so everyone just assumes you have it.

----

## Performance

<a href="https://modrinth.com/mod/alternate-current"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/alternate_current.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Alternate Current</a>
Efficient and non-locational redstone.<br>

<a href="https://modrinth.com/mod/lithium"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/lithium.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Lithium</a>
Improves TPS.<br>

<a href="https://modrinth.com/mod/servercore"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/servercore.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> ServerCore</a>
Server optimizations based on PaperMC [(which are disabled by default)](https://gitlab.com/ez-me/mc/-/blob/master/configs/servercore.toml)<br>

<a href="https://modrinth.com/mod/starlight"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/starlight.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Starlight</a>
Changes the lighting engine to be faster.<br>

<a href="https://modrinth.com/mod/ferrite-core"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/ferritecore.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> FerriteCore</a>
Reduces RAM usage.<br>

<a href="https://modrinth.com/mod/lazydfu"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/lazydfu.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> LazyDFU</a>
Makes "Data Fixer Upper" work only when neccesary.<br>

<a href="https://modrinth.com/mod/krypton"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/krypton.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Krypton</a>
Improves the Networking Stack.<br>

<a href="https://modrinth.com/mod/ksyxis"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/ksyxis.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Ksyxis</a>
Doesn't load 21x21 chunks around you everytime, only those necesary.<br>

<a href="https://modrinth.com/mod/c2me-fabric"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/c2me.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Concurrent Chunk Management</a>
Makes chunk loading faster by being multithreaded.<br>

<a href="https://modrinth.com/mod/worldspecificviewdistance"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/wsvd.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> World Specific View Distance</a>
Changes the render distance depending on Dimensions.<br>

<a href="https://www.curseforge.com/minecraft/mc-mods/fast-furnace-for-fabric"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/fast-furnace.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Fast Furnace</a>
Optimizes the furnaces behaviour.<br>

----

## Other

<a href="https://modrinth.com/mod/fabrictailor"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/fabrictailor.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Fabric Tailor</a>
Allow players to change their skin mid-game<br>

<a href="https://modrinth.com/mod/tabtps"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/tabtps.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> TabTPS</a>
Shows TPS, RAM, Ping.<br>

<a href="https://modrinth.com/mod/servertick"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/servertick.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Server Tick</a>
Allows Alt-F3 to work in servers.<br>

<a href="https://modrinth.com/mod/notenoughcrashes"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/nec.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Not Enough Crashes</a>
More useful stack traces for debugging.<br>

<a href="https://modrinth.com/mod/why-am-i-on-fire"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/waiof.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Why am I on Fire?</a>
Removes the visuals of fire when not necessary.<br>

<a href="https://modrinth.com/mod/image2map"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/image2map.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Image2Map</a>
Allows for easy map-art.<br>

----

## Extra Sensorial Perception

<a href="https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/xaero.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Xaero's Minimap</a>
Enhances the world detection for the clients.<br>

<a href="https://modrinth.com/mod/monsters-in-the-closet"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/mitc.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> Monsters in the Closet</a>
Highlights enemies which are not allowing you to sleep.<br>

----

## Anti Cheating

<a href="https://modrinth.com/mod/anti-xray"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/anti-xray.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> AntiXray</a>
Obscures how xray cheaters see the world.<br>

<a href="https://modrinth.com/mod/golfiv"><img src="https://gitlab.com/ez-me/mc/-/raw/master/media/mods/golfiv.webp" alt="[Logo]" loading="lazy" style="width:32px;height:32px;"> GolfIV</a>
Prevents many cheats from working properly or at all.<br>
