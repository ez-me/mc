# Notable Seeds

## Demos

[North Carolina](https://www.chunkbase.com/apps/seed-map#North%20Carolina)

Used in the demo world if you haven't purchased the game.

[glacier](https://www.chunkbase.com/apps/seed-map#glacier)

PC Gamer Demo world seed for beta 1.3

## Screen Panorama

[2151901553968352745](https://www.chunkbase.com/apps/seed-map#2151901553968352745)

Beta 1.8 onwards to Release 1.12 used in the main menu. Use version Beta 1.7

[1458140401](https://www.chunkbase.com/apps/seed-map#18w22a)

For 1.13 "Update Aquatic". Use version 18w22a

[2802867088795589976](https://www.chunkbase.com/apps/seed-map#2802867088795589976)

For 1.14 "Village & Pillage". Use version 18w48a

[-4404205509303106230](https://www.chunkbase.com/apps/seed-map#-4404205509303106230)

For 1.15 "Buzzy Bees". Use version 19w40a

[6006096527635909600](https://www.chunkbase.com/apps/seed-map#6006096527635909600)

For 1.16 "Nether Update". Use version 20w13a

## Alpha reliques

[3257840388504953787](https://www.chunkbase.com/apps/seed-map#3257840388504953787)

The default pack.png image. Use version alpha 1.2.2

[-6984854390176336655](https://www.chunkbase.com/apps/seed-map#-6984854390176336655)

Skull on Fire painting background. Use version alpha 1.1.2_01

[478868574082066804](https://www.chunkbase.com/apps/seed-map#478868574082066804)

The original Herobrine doctored screenshot. Use version alpha 1.0.16_02

