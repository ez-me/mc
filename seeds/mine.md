# Some of mine

<i>Why are there two editions of the same seed?</i>

I'm glad you asked! There are [shadow seeds](https://minecraft.fandom.com/wiki/Seed_%28level_generation%29#Shadow_seeds) which are same biomes, different everything else.

----

[107337](https://www.chunkbase.com/apps/seed-map#lol)

Has plethora of floating terrain pieces.

[-7379792620529013556](https://www.chunkbase.com/apps/seed-map#-7379792620529013556)

Has five Mansions not too far from spawn.

----

[-1931884077136427991](https://www.chunkbase.com/apps/seed-map#-1931884077136427991)

A lot of Desert Temples, Ocean Monuments and Igloos with Basements.

[-5447908543392478228](https://www.chunkbase.com/apps/seed-map#-5447908543392478228)

Has Pillager Outpost next to spawn instead of Village.

----

[-4150971215179034440](https://www.chunkbase.com/apps/seed-map#-4150971215179034440)

Floating terrain, some mansions far from spawn.

[-3228821405349871779](https://www.chunkbase.com/apps/seed-map#-3228821405349871779)

Mansions are a bit closer, many Ocean Ruins to the East.

----
